$('#name').focus(function () {
    $('#name-error').hide();
});

$('#phone').focus(function () {
    $('#phone-error').hide();
});
var validateName = function () {
    if ($('#name').val().length === 0) {
        $('#name-error').show();
        return false;
    }
    return true;
};
var validatePhone = function () {
    if ($('#phone').val().length === 0) {
        $('#phone-error').show();
        return false;
    }
    return true;
};
var validateFlag = function () {
    if (!$('#flag').prop('checked')) {
        $('#flag-error').show();
        return false;
    }
    return true;
};

$('#name').focusout(function () {
    var that = $(this);
    if (that.val().length === 0) {
        $('#name-error').show();
    }
});
$('#phone').focusout(function () {
    var that = $(this);
    if (that.val().length === 0) {
        $('#phone-error').show();
    }
});

$("#flag").change(function () {
    if (this.checked) {
        $('#flag-error').hide();
    }
});
var validateForm = function () {
    isValidName = validateName();
    isValidPhone = validatePhone();
    isValidFlag = validateFlag();
    return isValidName && isValidPhone && isValidFlag;
};

$('#submit-button').click(function handleSubmit(e) {
    var isValid = validateForm();
    if (!isValid) {
        e.stopImmediatePropagation();
    }
    else {
        yaCounter46174944.reachGoal('ZakazZvonka');
        createObject();
    }

});

